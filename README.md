# Tetsuo's challenge solution - Plotting a chart 📈

It was an enjoyable and the hardest job challenge I've ever done so far.
In my pre-coding analysis I've expected that the challenge was mostly about my React knowledge, boy I was wrong. 
After starting, I soon realized some requirement details required a deeper understanding of the problem to be perceived and couldn't be ignored, otherwise it would result in a wrong solution.
Based on that, I assumed that the solution was going to be heavly reliant on data manipulation methods so I've designed the code to be structured in a procedural and immutable way to keep it clean and protect my sanity. 
To handle huge amount of data I've splitted all methods into multiple async microsteps. 

Since the most valuable part of this challenge is the handling of the data and the code structure I tried to focus on using libs to get the UI done while avoiding creating those solutions myself.

## Constraints and assumptions
- Each line from the input holds one and only one JSON event.
- The user will not break rows inside the same JSON event.
- All JSON events will have its opening and closing brackets correct.
- The interval between the plot measures (X axis) is 1 minute. Should 2 or more events occur during this interval they will be plotted togheter.

## Third party modules
 - ```dirty-json@0.7.0``` (To bypass evaluating JSON-valid input formats)
 - ```plotly.js@1.47.2``` (Probably the chart lib that was intended to be used since the default styling is already similar to the one proposed in the challenge)
 - ```react-codemirror@1.0.0``` (Easy to setup clean textarea-based IDE)
 - ```react-reflex@3.0.13``` (Quick and customizable solution to resize divs)
 
## Result and project structure

### Workflow strategy
- Code the UI as simple as possible.
- Define data structure for Chart component and mock some data into it.
- Provide a library of methods to manipulate the inputted data into the planned data structure.
- Create methods in React to consume and manipulate the library.
- Testings and refactoring.
- Finish up styling and UI interactions.

I'm still slow when doing TDD and because of the narrow deadline I had to set I've decided not to use it. However, I've covered all methods with unit testing and created a few integration tests to make it more maintainable after I had a stable application.

### User Interface
It's not pixel perfect but it's pretty damn close.
![final_result](https://i.imgur.com/Y0ZpXua.png "Final UI")


### Structure
The project was bootstraped using ```create-react-app```. I've added a polyfill to make IE11 support ```Array.prototype.find``` method. I'm still using ```react-scripts``` (Jest) for testing. Every relevant method have been documented inline using JSDoc.

For the UI I've designed the solution using four React components:
- ```index.js``` - Entry point of the application
- ```App.js``` -  Stateful component with all data and provider methods. Also the interface between React and the ```Chart``` instance.
- ```ide.js``` - Functional component to hold ```code-mirror``` instance.
- ```chart.js``` - Functional component with methods to render the ```plotly.js``` instance correctly.

For the methods library I've created an external file named ```create-chart.js```. I based the code structure on module pattern to provide a clean code, making it easier to understand. The library returns an class-like object with a constructor and some methods to manipulate the data. Since we make an instance of it, it has an internal state but I've decided to use it only to store boundaries calculations. That's because the challenge was based on user input and not on live feed so I have no need to store the previous states. Nevertheless, this design makes it easy to refactor to accept a live feed.

## Instalation and runnning
Patch into the challenge repository and run:
```npm install```

To run just type:
```npm start```





