/**
 * The examples provided are not JSON-valid syntax.
 * Since it's useful to use the data as real Javascript Object,
 * instead of using a string representation of a JSON I've decided to use
 * dirty-json to bypass the need of reformatting the input.
 */
import dJSON from 'dirty-json'

/**
 * The strategy behind using module pattern
 * is to provide a clean code structure to all
 * the required methods. The main challenge was to
 * correctly manipulate the provided data in order
 * to have it consumed by plotly.js.
 * 
 * I tried to keep a immutable, non-blocking, procedural approach in
 * this solution and designed it to be easily refactored to
 * receive live input feed in the future.
 */
const createChart = (()=>{

    let that 

    const Chart = function() {

        // Initialize boundaries with non-blocking range
        this.boundaries = {
            min: 0,
            max: Infinity
        }
        // Give access to "this" to all methods
        that = this 
    }

/** 
 * 
 * Converts a sequence of events to a plotly.js valid data structure.
 * 
 * @async
 * @public
 * @function parseGraphData
 * @param {Array} events - Array containing a sequence of events.
 * @returns {Promise<Array>} Promise object with plottable data. 
 */
    const parseChartData = data => {
        return new Promise(async (resolve, reject) => {
            try {
                // Parsing and filtering of text input
                let parsedData  = await parseJSON(data)
                let events      = await filterLastDataSequence(parsedData)

                // Desconstructing assignment for clean code
                let [selectSet, groupSet, span, dataSet] = 
                [
                    events[0].select,   // Select fields from query 
                    events[0].group,    // Group fields from query
                    parsedData          // Retrieve the last span 
                        .filter(event => event.type === 'span').pop(), 
                    events              // All data after last start 
                        .filter(event => event.type === 'data') 
                ]  

                if (!span) reject(new Error('No span event provided.'))

                // Set new broundaries based on the latest input
                that.boundaries = {
                    min: span.begin,
                    max: span.end
                }

                // Restructure data to suit chart component
                let joined      = await joinGroups(dataSet, groupSet)
                let values      = await joinSetsByGroups(joined, selectSet)
                let cleaned     = await cleanData(values, groupSet, selectSet)
                
                resolve(cleaned)

            } catch(e) { reject(e) } 
        })
    }

/** 
 * 
 * Reads string extracted from textarea and returns an Array of event objects.
 * 
 * @async
 * @function parseJSON
 * @param {String} data - String containing the data separated by \n.
 * @returns {Promise<Array>} Promise object with the array of parsed events. 
 */
    const parseJSON = data => {
        return new Promise((resolve, reject) => {
            let parsedJSON = data
                .split('\n') // Remove line breaks
                .filter(event => event !== '')  // Remove empty lines
                .map(event => dJSON.parse(event, reject)) // Parse lines, reject on error fallback
                .filter(event => 
                    typeof(event) === 'object' // Remove anything that isn't an object
                    && event.hasOwnProperty('type') // Assure the mandatory type field
                    && event.hasOwnProperty('timestamp') // Assure the mandatory timestamp field
                )

            resolve(parsedJSON)
        })
    }

/** 
* 
* Returns the last valid sequence of events.
* 
* @async
* @function filterLastDataSequence
* @param {Array} data - Array containing the events in valid format.
* @returns {Promise<Array>} Promise object with the sliced array of events. 
*/
    const filterLastDataSequence = data => {
        return new Promise((resolve, reject) => {
            let lastStartOccurence  = findLastOccurence(event => event.type === 'start', data)
            let lastStopOccurence   = findLastOccurence(event => event.type === 'stop', data)
            if (lastStartOccurence === -1) reject(new Error('No start event provided.'))
            if (lastStopOccurence === -1 // Check if there are no stop events
                || lastStopOccurence < lastStartOccurence) resolve(data.slice(lastStartOccurence, data.length))
            else resolve(data.slice(lastStartOccurence, lastStopOccurence+1))
        })
    }

/**
* Returns the index of the last occurence of an element within an array.
* That should be unecessary if we had Array.prototype.findLastIndex with callback.
* Since I couldn't find any native solutions I've made this work around method.
*/
    const findLastOccurence = (predicate, array) => {
        let lastOcurrance = array
            .reduce((acc, element, index) => predicate(element) ? [...acc,index] : acc, []) // Find all events with type start
            .pop() // Returns the last found index
        if (lastOcurrance >= 0) return lastOcurrance
        else return -1
    }
    
/** 
* 
* Map data events and add 'groups' keys.
* 
* @async
* @function joinGroups
* @param {Array} data - Array containing the events objects in valid format.
* @param {Array} groupSet - Array containing the strings with group names.
* @returns {Promise<Array>} Promise object with data containing joined groups key. 
*/
    const joinGroups = (data, groupSet) => {
        return new Promise((resolve)=>{
            let joinedGroup = data.map(event => {
                return {
                    ...event,
                    groups: groupSet.reduce((acc,element) => event[element] ? [...acc, event[element]] : acc, [])
                }
            })
            resolve(joinedGroup)
        })
    }

/** 
* 
* Destruct data events and returns a new data collection joined by groups with values.
* This could be splitted into 2 methods.
* 
* @async
* @function joinSetsByGroups
* @param {Array} data - Array containing the events objects in valid format. Must contain 'groups' key.
* @param {Array} selectSet - Array containing the strings with select names.
* @returns {Promise<Array>} Promise object with data containing joined values. 
*/
    const joinSetsByGroups = (data, selectSet) => {
        return new Promise((resolve, reject) => {
            let parsed = data.reduce((acc, element) => {

                // Find events with repeated groups field
                let matchingGroup = acc.find(event => 
                    JSON.stringify(event.groups.sort()) === JSON.stringify(element.groups.sort()))

                // Get index from events with repeated groups field
                let matchingGroupIndex = acc.indexOf(matchingGroup)

                selectSet.forEach(select => {

                    // Struct value key to be added
                    let value = {timestamp: element.timestamp, [select]: element[select]}

                    // Check if current element has repeating group field
                    if (matchingGroupIndex === -1) {
                        // Add new element with new value key
                        acc.push({...element, name: select, values: [value]})
                    } else {
                        // Find events with repeated select field
                        let matchingSelect = acc.find(event => 
                                event.name === select
                            &&  JSON.stringify(event.groups.sort()) === JSON.stringify(element.groups.sort()))

                        // Get index from events with repeated select field
                        let matchingSelectIndex = acc.indexOf(matchingSelect)

                        // Check if current element has repeating select field
                        if (matchingGroupIndex === -1) {
                            // Add new element with new value key
                            acc.push({...element, name: select, values: [value]})
                        } else {
                            // Mutate existing element with matching fields
                            acc[matchingSelectIndex].values.push(value)
                        }
                    }
                })
                return acc
            },[])

            resolve(parsed)
        })
    }

/** 
* 
* Remove unnecessary keys from data sequence.
* 
* @async
* @function cleanData
* @param {Array} data - Array containing the events objects in valid format.
* @param {Array} groupSet - Array containing the strings with group names.
* @param {Array} selectSet - Array containing the strings with select names.
* @returns {Promise<Array>} Promise object with updated data without provided sets keys. 
*/
    const cleanData = (data, groupSet, selectSet) => {
        return new Promise((resolve, reject)=> {
            let cleanedData = data
                .filter(events => events.groups.length)
                .map(event => {
                    delete event.timestamp
                    groupSet.forEach(group => delete event[group])
                    selectSet.forEach(select => delete event[select])
                    return event
                })
            resolve(cleanedData)
        })
        
    }


/** 
* Expose methods to testing interface
*/
    const test = (() => {
        return {
            parseJSON,
            filterLastDataSequence
        }
    })()

/** 
* Expose methods and props
*/
    Chart.prototype = {
        constructor: Chart,
        parse: input => parseChartData(input),
        getBoundaries: () => that.boundaries,
        test
    }

    return Chart
        
})()

export default createChart

