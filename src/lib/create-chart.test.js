import createChart from './create-chart'

describe('unit tests', () => {

    let Chart = new createChart()
    let SAMPLE_INPUT = 
        `{type: 'start', timestamp : 1519862400000, select: ['min_response_time', 'max_response_time'], group: ['os', 'browser']}
        {type: 'span', timestamp: 1519862900000, begin: 1519862900000, end: 1519862960000}`

    it('should have default boundaries', () => {
        let boundaries = Chart.getBoundaries()
        expect(boundaries).toEqual({
            min: 0,
            max: Infinity
        })
    })
    
    it('should calculate boundaries based on last span', async () => {
        await Chart.parse(SAMPLE_INPUT)
        let boundaries = Chart.getBoundaries()
        expect(boundaries).toEqual({
            min: 1519862900000,
            max: 1519862960000
        })
        
    })
    
    it('should parse invalid json', async () => {
        let input = 
            `{type: 'foo', timestamp: 1234}
            {"type": 'bar', timestamp: 4321}`
        let parsedInput = await Chart.test.parseJSON(input)
        expect(parsedInput).toEqual([
            {type: 'foo', timestamp: 1234},
            {type: 'bar', timestamp: 4321}
        ])
    })
    
    it('should ignore data without mandatory fields', async () => {
        let input = 
            `{type: 'foo'}
            {type: 'bar'}`
        let parsedInput = await Chart.test.parseJSON(input)
        expect(parsedInput).toEqual([])
    })
    
    it('should reject data without start type field', async () => {
        let input = [
            {type: 'stat'},
            {type: 'span'}
        ]
        try {
            await Chart.test.filterLastDataSequence(input)
        } catch(e){
            expect(e.message).toEqual('No start event provided.')
        }
    })
    
    it('should reject data without span type field', async () => {
        let input = 
            `{type: 'start', timestamp : 1519862400000, select: ['min_response_time', 'max_response_time'], group: ['os', 'browser']}
            {type: 'spn', timestamp: 1519862900000, begin: 1519862900000, end: 1519862960000}`
        try {
            await Chart.parse(input)
        } catch(e){
            expect(e.message).toEqual('No span event provided.')
        }
    })
})

describe('integration tests', async () => {

    let Chart = new createChart()

    let SAMPLE_INPUT = 
    `{type: 'start', timestamp : 1519862400000, select: ['min_response_time', 'max_response_time'], group: ['os', 'browser']}
    {type: 'span', timestamp: 1519862400000, begin: 1519862400000, end: 1519862460000}
    {type: 'data', timestamp: 1519862400000, os: 'linux', browser: 'chrome', min_response_time: 0.1, max_response_time: 1.3}
    {type: 'data', timestamp: 1519862400000, os: 'mac', browser: 'chrome', min_response_time: 0.2, max_response_time: 1.2}
    {type: 'data', timestamp: 1519862400000, os: 'mac', browser: 'firefox', min_response_time: 0.3, max_response_time: 1.2}
    {type: 'data', timestamp: 1519862400000, os: 'linux', browser: 'firefox', min_response_time: 0.1, max_response_time: 1.0}
    {type: 'data', timestamp: 1519862460000, os: 'linux', browser: 'chrome', min_response_time: 0.2, max_response_time: 0.9}
    {type: 'data', timestamp: 1519862460000, os: 'mac', browser: 'chrome', min_response_time: 0.1, max_response_time: 1.0}
    {type: 'data', timestamp: 1519862460000, os: 'mac', browser: 'firefox', min_response_time: 0.2, max_response_time: 1.1}
    {type: 'data', timestamp: 1519862460000, os: 'linux', browser: 'firefox', min_response_time: 0.3, max_response_time: 1.4}
    {type: 'stop', timestamp: 1519862460000}`


    it('should create 1 entry for each individual group pair', async () => {
        let data = await Chart.parse(SAMPLE_INPUT)
        expect(data.length).toEqual(8)
    })

    it('should have values field in every element', async () => {
        let data = await Chart.parse(SAMPLE_INPUT)
        let check = data.reduce((acc, element) => 'values' in element ? acc : false, true)
        expect(check).toEqual(true)
    })

    it('should format data correctly', async () => {
        let data = await Chart.parse(SAMPLE_INPUT)
        let expected = `[{"type":"data","groups":["chrome","linux"],"name":"min_response_time","values":[{"timestamp":1519862400000,"min_response_time":0.1},{"timestamp":1519862460000,"min_response_time":0.2}]},{"type":"data","groups":["chrome","linux"],"name":"max_response_time","values":[{"timestamp":1519862400000,"max_response_time":1.3},{"timestamp":1519862460000,"max_response_time":0.9}]},{"type":"data","groups":["chrome","mac"],"name":"min_response_time","values":[{"timestamp":1519862400000,"min_response_time":0.2},{"timestamp":1519862460000,"min_response_time":0.1}]},{"type":"data","groups":["chrome","mac"],"name":"max_response_time","values":[{"timestamp":1519862400000,"max_response_time":1.2},{"timestamp":1519862460000,"max_response_time":1}]},{"type":"data","groups":["firefox","mac"],"name":"min_response_time","values":[{"timestamp":1519862400000,"min_response_time":0.3},{"timestamp":1519862460000,"min_response_time":0.2}]},{"type":"data","groups":["firefox","mac"],"name":"max_response_time","values":[{"timestamp":1519862400000,"max_response_time":1.2},{"timestamp":1519862460000,"max_response_time":1.1}]},{"type":"data","groups":["firefox","linux"],"name":"min_response_time","values":[{"timestamp":1519862400000,"min_response_time":0.1},{"timestamp":1519862460000,"min_response_time":0.3}]},{"type":"data","groups":["firefox","linux"],"name":"max_response_time","values":[{"timestamp":1519862400000,"max_response_time":1},{"timestamp":1519862460000,"max_response_time":1.4}]}]`
        expect(JSON.stringify(data)).toEqual(expected)
    })
})
