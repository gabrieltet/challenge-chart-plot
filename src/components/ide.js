import React from 'react';
import CodeMirror from 'react-codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import './ide-monokai.css'


const IDE = props => {
  const options = {
    lineNumbers: true,
    mode: {name: 'javascript', json: true},
    theme: 'monokai',
    lineWrapping: true,
    viewportMargin: Infinity
  }

  return(
    <CodeMirror
      onChange={props.changeData}
      options={options}
    />
  )
}

export default IDE;
