import React from 'react'
import Plot from 'react-plotly.js'

const Chart = props => {
  
  const parseData = data => {
    return data.map(event => {
      let sortedValues = event.values
        .sort((a, b) => a.timestamp - b.timestamp )
        .map(differenceFromTimestamps)
      return {
        x: sortedValues
          .filter(checkBoundaries)
          .map(formatTime),
        y: sortedValues
          .filter(checkBoundaries)
          .map(value => value[event.name]),
        type: 'scatter',
        name: formatName(event.name, event.groups)
      }
    })
  }

  const formatTime = value => {
    return  padLeftDigit(millisecondsToHour(value.diffInMilliseconds))
            + ':' +
            padLeftDigit(millisecondsToMinute(value.diffInMilliseconds))
  }

  const checkBoundaries = value => {
    return  value.timestamp     >= props.boundaries.min 
            && value.timestamp  <= props.boundaries.max
  }

  const differenceFromTimestamps = (value, index, arr) => {
    if (index === 0) value.diffInMilliseconds = 0
    else value.diffInMilliseconds = Math.abs(value.timestamp - arr[0].timestamp)
    return value
  }

  const millisecondsToMinute = time => {
    return ~~((time)/60000)%60
  }

  const millisecondsToHour = time => {
    return ~~((time)/3600000)
  }

  const padLeftDigit = number => {
    return (number < 10 ? '0' : '') + number
  }

  const formatName = (name, groups) => {
    let prefix = groups.map(group => group.charAt(0).toUpperCase() + group.slice(1)).join(' ')
    let suffix = name.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
    return [prefix, suffix].join(' ')
  }

  return (
      <Plot
        data={parseData(props.data)}
        useResizeHandler={true}
        style={{height: props.minGraphHeight}}
        config={ {displayModeBar: false, staticPlot: false,} }
        layout={ {
          margin: {
            pad: 50,
            t: 20,
            l: 20,
            r: 20
          },
          autosize: true, 
          yaxis: {showgrid: true, showticklabels: false},
          xaxis: {showgrid: false}
        } }
      />
  )
}

export default Chart