import React, { Component } from 'react'
import './App.css'
import 'react-reflex/styles.css'
import IDE from './components/ide'
import Chart from './components/chart'
import {
  ReflexContainer,
  ReflexSplitter,
  ReflexElement
} from 'react-reflex'
import createChart from './lib/create-chart'

class App extends Component {
  constructor(props){
    super(props)
    this.Chart = new createChart()
    this.state = {
      input: '',
      data: [],
      boundaries: this.Chart.getBoundaries(),
      error: '',
      minGraphHeight: '40vh'
    }
  }

  changeData = input => this.setState({input})
  
  submitData = async () => {
    try {
      const graphData = await this.Chart.parse(this.state.input)
      const boundaries = this.Chart.getBoundaries()
      this.setState({data: graphData, boundaries, error: ''})
    } catch(e) {
      this.setState({error: e.message})
      alert(e.message)
    }
  }

  setMinGraphHeight = height => {
    if (this.state.minGraphHeight !== height) this.setState({minGraphHeight: height})
    return height
  }

  render() {
    return (
      <div className="App">
        <header className="Header">
          Tetsuo's Challenge
        </header>
        <ReflexContainer orientation="horizontal">
          <ReflexElement>
            <IDE changeData={this.changeData} />
          </ReflexElement>

          <ReflexSplitter className='customSplitter'> 
            <div className="dragHandle"></div> 
          </ ReflexSplitter>

          <ReflexElement 
            className="bottom-pane"
            propagateDimensions
            onStopResize={child => this.setMinGraphHeight(child.domElement.offsetHeight)}
            propagateDimensionsRate={10}
            minSize={200}
          >
            {
              this.state.data.length
              ? (<Chart data={this.state.data} minGraphHeight={this.state.minGraphHeight} boundaries={this.state.boundaries} />)
              : (<div style={{width: '100%', height: '100%'}}>
                  {this.state.error || 'No data inputed.'}
                </div>)
            }
          </ReflexElement>
        </ReflexContainer>
        <footer className="Footer">
          <button type="button" onClick={this.submitData} className="btn btn-primary">GENERATE CHART</button>
        </footer>
      </div>
    );
  }
}

export default App;
